package betgame;
import javafx.beans.value.ChangeListener;
import javafx.application.*;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;



import javax.swing.event.*;
import javafx.application.*;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class BetApplication extends Application {
	private BetPlay round=new BetPlay();
	@Override
	public void start(Stage stage) {
		Group root = new Group(); 
		HBox buttons= new HBox();
		Button button1 = new Button("small");
		Button button2 = new Button("large");
		
		buttons.getChildren().addAll(button1, button2);
		HBox textfields= new HBox();
		
		TextField message = new TextField("wellcome!");
		TextField money = new TextField("500");
		TextField betamount = new TextField();
		 betamount.textProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable,String old, String newV) {
				if (Integer.parseInt(newV)<round.getMoney()) {
					System.out.print("wrong Bet");
				}
				else
			round.setBet(Integer.parseInt(newV));
			}});

		textfields.getChildren().addAll(message,money,betamount);
		message.setPrefWidth(400);
		VBox overall=new VBox();
		
		overall.getChildren().addAll(buttons,textfields);
		root.getChildren().add(overall);
		
		BetChoice actionObject1 = new BetChoice(message,money,betamount,"small",this.round);
		BetChoice actionObject2 = new BetChoice(message,money,betamount,"large",this.round);
		
		button1.setOnAction(actionObject1);
		button2.setOnAction(actionObject2);
		
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
//associate scene to stage and show
		stage.setTitle("betgame"); 
		stage.setScene(scene); 
		stage.show(); 
		}
	 public static void main(String[] args) {
        Application.launch(args);
    }
}    


