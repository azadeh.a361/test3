package betgame;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
public  class BetChoice implements EventHandler<ActionEvent> {
	private TextField message;
	private TextField money;
	private TextField bet;
	private String input;
	private BetPlay gameround=new BetPlay();
	
	public BetChoice(TextField message,TextField money ,TextField bet ,String  input ,BetPlay gameround) {
		this.message=message;
this.money=money;
this.bet=bet;
this.input=input;

this.gameround=gameround;

}
	public void handle (ActionEvent e) {
		
		this.money.setText(""+this.gameround.playRound(this.input));
		this.bet.setText(""+this.gameround.getBet());
		this.message.setText(" you bet  "+this.gameround.getBet()+"you chose  "
				+ this.input +" and you "+this.gameround.ifWIn(this.input));
		
	}
	
	}